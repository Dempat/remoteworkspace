// Made with Blockbench 4.9.2
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports

public class Modeldungeon_skele_1<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in
	// the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(
			new ResourceLocation("modid", "dungeon_skele_1"), "main");
	private final ModelPart Skeleton;

	public Modeldungeon_skele_1(ModelPart root) {
		this.Skeleton = root.getChild("Skeleton");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition Skeleton = partdefinition.addOrReplaceChild("Skeleton", CubeListBuilder.create(),
				PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition Torsoinf = Skeleton.addOrReplaceChild("Torsoinf", CubeListBuilder.create(),
				PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition PiernaIzq = Torsoinf.addOrReplaceChild("PiernaIzq", CubeListBuilder.create().texOffs(18, 24)
				.addBox(3.0F, -8.0F, -2.0F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)),
				PartPose.offset(-1.0F, 0.0F, 0.0F));

		PartDefinition PiernaDer = Torsoinf.addOrReplaceChild("PiernaDer", CubeListBuilder.create().texOffs(22, 24)
				.addBox(-3.0F, -8.0F, -2.0F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)),
				PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Pelvis = Torsoinf.addOrReplaceChild("Pelvis",
				CubeListBuilder.create().texOffs(0, 0)
						.addBox(-3.0F, -10.0F, -3.0F, 6.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)).texOffs(7, 19)
						.addBox(-1.0F, -8.0F, -3.0F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)).texOffs(10, 24)
						.addBox(1.0F, -9.0F, -3.0F, 1.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)).texOffs(24, 3)
						.addBox(-2.0F, -9.0F, -3.0F, 1.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)),
				PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Craneo = Skeleton.addOrReplaceChild("Craneo",
				CubeListBuilder.create().texOffs(21, 14)
						.addBox(-1.0F, -20.0F, -3.0F, 2.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)).texOffs(14, 10)
						.addBox(-2.0F, -23.0F, -3.0F, 4.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)).texOffs(17, 20)
						.addBox(-1.0F, -18.0F, -3.0F, 2.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)).texOffs(28, 12)
						.addBox(-3.0F, -20.0F, -3.0F, 1.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)).texOffs(15, 0)
						.addBox(-1.0F, -22.0F, 0.0F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(25, 8)
						.addBox(2.0F, -20.0F, -3.0F, 1.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)).texOffs(0, 17)
						.addBox(-1.0F, -21.0F, -4.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)).texOffs(15, 15)
						.addBox(0.0F, -21.0F, -4.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)).texOffs(7, 12)
						.addBox(-3.0F, -22.0F, -4.0F, 1.0F, 2.0F, 5.0F, new CubeDeformation(0.0F)).texOffs(0, 10)
						.addBox(2.0F, -22.0F, -4.0F, 1.0F, 2.0F, 5.0F, new CubeDeformation(0.0F)).texOffs(18, 5)
						.addBox(1.0F, -19.0F, -4.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)).texOffs(17, 0)
						.addBox(-2.0F, -19.0F, -4.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)).texOffs(23, 0)
						.addBox(-2.0F, -20.0F, 0.0F, 4.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(24, 19)
						.addBox(-2.0F, -20.0F, -4.0F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(7, 10)
						.addBox(-2.0F, -22.0F, -4.0F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(14, 14)
						.addBox(1.0F, -21.0F, -3.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(0, 12)
						.addBox(-2.0F, -21.0F, -3.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(7, 12)
						.addBox(-2.0F, -17.0F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(0, 10)
						.addBox(1.0F, -17.0F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(14, 20)
						.addBox(-1.0F, -23.0F, -4.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)),
				PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition TorsoSup = Skeleton.addOrReplaceChild("TorsoSup", CubeListBuilder.create(),
				PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Body = TorsoSup.addOrReplaceChild("Body",
				CubeListBuilder.create().texOffs(0, 22)
						.addBox(-1.0F, -17.0F, -2.0F, 2.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(0, 8)
						.addBox(-4.0F, -14.0F, -1.0F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(0, 6)
						.addBox(-4.0F, -12.0F, -1.0F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(0, 4)
						.addBox(-4.0F, -16.0F, -1.0F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(30, 6)
						.addBox(-4.0F, -14.0F, -3.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(0, 30)
						.addBox(-4.0F, -12.0F, -3.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(29, 21)
						.addBox(-4.0F, -16.0F, -3.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(29, 16)
						.addBox(3.0F, -14.0F, -3.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(10, 29)
						.addBox(3.0F, -16.0F, -3.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(29, 3)
						.addBox(3.0F, -12.0F, -3.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(25, 32)
						.addBox(1.0F, -12.0F, -4.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(9, 32)
						.addBox(-3.0F, -12.0F, -4.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(30, 30)
						.addBox(1.0F, -14.0F, -4.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(30, 28)
						.addBox(-3.0F, -14.0F, -4.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(30, 26)
						.addBox(1.0F, -16.0F, -4.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(30, 24)
						.addBox(-3.0F, -16.0F, -4.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)),
				PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition BrazoIqz = TorsoSup.addOrReplaceChild("BrazoIqz", CubeListBuilder.create().texOffs(30, 9).addBox(
				-6.0F, -16.0F, -2.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Brazo1 = BrazoIqz.addOrReplaceChild("Brazo1", CubeListBuilder.create().texOffs(26, 23)
				.addBox(-1.001F, -0.0433F, -1.0415F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)),
				PartPose.offset(6.0F, -15.0F, -1.0F));

		PartDefinition BrazoDer = TorsoSup.addOrReplaceChild("BrazoDer",
				CubeListBuilder.create().texOffs(6, 24)
						.addBox(-6.0F, -15.0F, -2.0F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(24, 21)
						.addBox(4.0F, -16.0F, -2.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)),
				PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Brazo2 = BrazoDer.addOrReplaceChild("Brazo2", CubeListBuilder.create(),
				PartPose.offset(0.0F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay,
			float red, float green, float blue, float alpha) {
		Skeleton.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw,
			float headPitch) {
	}
}