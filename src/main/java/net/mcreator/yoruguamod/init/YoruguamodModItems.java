
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.yoruguamod.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.common.ForgeSpawnEggItem;

import net.minecraft.world.item.Item;

import net.mcreator.yoruguamod.item.PicorubyItem;
import net.mcreator.yoruguamod.item.Martillo3x3Item;
import net.mcreator.yoruguamod.item.GuadanaItem;
import net.mcreator.yoruguamod.item.EspadaNoBugItem;
import net.mcreator.yoruguamod.item.ElucidatorItem;
import net.mcreator.yoruguamod.item.DagadashItem;
import net.mcreator.yoruguamod.YoruguamodMod;

public class YoruguamodModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, YoruguamodMod.MODID);
	public static final RegistryObject<Item> PICORUBY = REGISTRY.register("picoruby", () -> new PicorubyItem());
	public static final RegistryObject<Item> ESPADA_NO_BUG = REGISTRY.register("espada_no_bug", () -> new EspadaNoBugItem());
	public static final RegistryObject<Item> DAGADASH = REGISTRY.register("dagadash", () -> new DagadashItem());
	public static final RegistryObject<Item> MARTILLO_3X_3 = REGISTRY.register("martillo_3x_3", () -> new Martillo3x3Item());
	public static final RegistryObject<Item> ELUCIDATOR = REGISTRY.register("elucidator", () -> new ElucidatorItem());
	public static final RegistryObject<Item> BLOQUESIN_SPAWN_EGG = REGISTRY.register("bloquesin_spawn_egg", () -> new ForgeSpawnEggItem(YoruguamodModEntities.BLOQUESIN, -1, -1, new Item.Properties()));
	public static final RegistryObject<Item> DUNGEON_SKELETON_SPAWN_EGG = REGISTRY.register("dungeon_skeleton_spawn_egg", () -> new ForgeSpawnEggItem(YoruguamodModEntities.DUNGEON_SKELETON, -16777216, -6750208, new Item.Properties()));
	public static final RegistryObject<Item> BLOQUESIN_L_SPAWN_EGG = REGISTRY.register("bloquesin_l_spawn_egg", () -> new ForgeSpawnEggItem(YoruguamodModEntities.BLOQUESIN_L, -1, -1, new Item.Properties()));
	public static final RegistryObject<Item> COLOSO_SPAWN_EGG = REGISTRY.register("coloso_spawn_egg", () -> new ForgeSpawnEggItem(YoruguamodModEntities.COLOSO, -10066330, -1, new Item.Properties()));
	public static final RegistryObject<Item> GUADANA = REGISTRY.register("guadana", () -> new GuadanaItem());
}
