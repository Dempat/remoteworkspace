
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.yoruguamod.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;

import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Entity;

import net.mcreator.yoruguamod.entity.DungeonSkeletonEntity;
import net.mcreator.yoruguamod.entity.ColosoEntityProjectile;
import net.mcreator.yoruguamod.entity.ColosoEntity;
import net.mcreator.yoruguamod.entity.BloquesinLEntity;
import net.mcreator.yoruguamod.entity.BloquesinEntity;
import net.mcreator.yoruguamod.YoruguamodMod;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class YoruguamodModEntities {
	public static final DeferredRegister<EntityType<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.ENTITY_TYPES, YoruguamodMod.MODID);
	public static final RegistryObject<EntityType<BloquesinEntity>> BLOQUESIN = register("bloquesin", EntityType.Builder.<BloquesinEntity>of(BloquesinEntity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64)
			.setUpdateInterval(3).setCustomClientFactory(BloquesinEntity::new).fireImmune().sized(0.5f, 0.5f));
	public static final RegistryObject<EntityType<DungeonSkeletonEntity>> DUNGEON_SKELETON = register("dungeon_skeleton",
			EntityType.Builder.<DungeonSkeletonEntity>of(DungeonSkeletonEntity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(DungeonSkeletonEntity::new)

					.sized(0.6f, 1.8f));
	public static final RegistryObject<EntityType<BloquesinLEntity>> BLOQUESIN_L = register("bloquesin_l",
			EntityType.Builder.<BloquesinLEntity>of(BloquesinLEntity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(BloquesinLEntity::new)

					.sized(0.3f, 0.3f));
	public static final RegistryObject<EntityType<ColosoEntity>> COLOSO = register("coloso",
			EntityType.Builder.<ColosoEntity>of(ColosoEntity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(ColosoEntity::new)

					.sized(5f, 5f));
	public static final RegistryObject<EntityType<ColosoEntityProjectile>> COLOSO_PROJECTILE = register("projectile_coloso", EntityType.Builder.<ColosoEntityProjectile>of(ColosoEntityProjectile::new, MobCategory.MISC)
			.setShouldReceiveVelocityUpdates(true).setTrackingRange(64).setUpdateInterval(1).setCustomClientFactory(ColosoEntityProjectile::new).sized(0.5f, 0.5f));

	private static <T extends Entity> RegistryObject<EntityType<T>> register(String registryname, EntityType.Builder<T> entityTypeBuilder) {
		return REGISTRY.register(registryname, () -> (EntityType<T>) entityTypeBuilder.build(registryname));
	}

	@SubscribeEvent
	public static void init(FMLCommonSetupEvent event) {
		event.enqueueWork(() -> {
			BloquesinEntity.init();
			DungeonSkeletonEntity.init();
			BloquesinLEntity.init();
			ColosoEntity.init();
		});
	}

	@SubscribeEvent
	public static void registerAttributes(EntityAttributeCreationEvent event) {
		event.put(BLOQUESIN.get(), BloquesinEntity.createAttributes().build());
		event.put(DUNGEON_SKELETON.get(), DungeonSkeletonEntity.createAttributes().build());
		event.put(BLOQUESIN_L.get(), BloquesinLEntity.createAttributes().build());
		event.put(COLOSO.get(), ColosoEntity.createAttributes().build());
	}
}
