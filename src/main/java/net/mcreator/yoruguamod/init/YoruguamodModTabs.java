
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.yoruguamod.init;

import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.BuildCreativeModeTabContentsEvent;

import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.core.registries.Registries;

import net.mcreator.yoruguamod.YoruguamodMod;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class YoruguamodModTabs {
	public static final DeferredRegister<CreativeModeTab> REGISTRY = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, YoruguamodMod.MODID);

	@SubscribeEvent
	public static void buildTabContentsVanilla(BuildCreativeModeTabContentsEvent tabData) {

		if (tabData.getTabKey() == CreativeModeTabs.COMBAT) {
			tabData.accept(YoruguamodModItems.ESPADA_NO_BUG.get());
			tabData.accept(YoruguamodModItems.DAGADASH.get());
			tabData.accept(YoruguamodModItems.GUADANA.get());
		}

		if (tabData.getTabKey() == CreativeModeTabs.SPAWN_EGGS) {
			tabData.accept(YoruguamodModItems.BLOQUESIN_SPAWN_EGG.get());
			tabData.accept(YoruguamodModItems.DUNGEON_SKELETON_SPAWN_EGG.get());
			tabData.accept(YoruguamodModItems.BLOQUESIN_L_SPAWN_EGG.get());
			tabData.accept(YoruguamodModItems.COLOSO_SPAWN_EGG.get());
		}

		if (tabData.getTabKey() == CreativeModeTabs.TOOLS_AND_UTILITIES) {
			tabData.accept(YoruguamodModItems.PICORUBY.get());
			tabData.accept(YoruguamodModItems.MARTILLO_3X_3.get());
			tabData.accept(YoruguamodModItems.ELUCIDATOR.get());
		}
	}
}
