
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.yoruguamod.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.mcreator.yoruguamod.client.model.Modeldungeon_skele_1;
import net.mcreator.yoruguamod.client.model.ModelBotasSaltos;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = {Dist.CLIENT})
public class YoruguamodModModels {
	@SubscribeEvent
	public static void registerLayerDefinitions(EntityRenderersEvent.RegisterLayerDefinitions event) {
		event.registerLayerDefinition(ModelBotasSaltos.LAYER_LOCATION, ModelBotasSaltos::createBodyLayer);
		event.registerLayerDefinition(Modeldungeon_skele_1.LAYER_LOCATION, Modeldungeon_skele_1::createBodyLayer);
	}
}
