
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.yoruguamod.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.client.renderer.entity.ThrownItemRenderer;

import net.mcreator.yoruguamod.client.renderer.DungeonSkeletonRenderer;
import net.mcreator.yoruguamod.client.renderer.ColosoRenderer;
import net.mcreator.yoruguamod.client.renderer.BloquesinRenderer;
import net.mcreator.yoruguamod.client.renderer.BloquesinLRenderer;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class YoruguamodModEntityRenderers {
	@SubscribeEvent
	public static void registerEntityRenderers(EntityRenderersEvent.RegisterRenderers event) {
		event.registerEntityRenderer(YoruguamodModEntities.BLOQUESIN.get(), BloquesinRenderer::new);
		event.registerEntityRenderer(YoruguamodModEntities.DUNGEON_SKELETON.get(), DungeonSkeletonRenderer::new);
		event.registerEntityRenderer(YoruguamodModEntities.BLOQUESIN_L.get(), BloquesinLRenderer::new);
		event.registerEntityRenderer(YoruguamodModEntities.COLOSO.get(), ColosoRenderer::new);
		event.registerEntityRenderer(YoruguamodModEntities.COLOSO_PROJECTILE.get(), ThrownItemRenderer::new);
	}
}
