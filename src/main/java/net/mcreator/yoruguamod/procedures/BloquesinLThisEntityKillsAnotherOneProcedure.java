package net.mcreator.yoruguamod.procedures;

import net.minecraft.world.level.LevelAccessor;
import net.minecraft.network.chat.Component;

public class BloquesinLThisEntityKillsAnotherOneProcedure {
	public static void execute(LevelAccessor world) {
		if (!world.isClientSide() && world.getServer() != null)
			world.getServer().getPlayerList().broadcastSystemMessage(Component.literal("\u00A1Pap\u00E1 mat\u00E9 a un plata!"), false);
	}
}
