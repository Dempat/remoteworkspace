package net.mcreator.yoruguamod.entity.model;

import software.bernie.geckolib.model.GeoModel;

import net.minecraft.resources.ResourceLocation;

import net.mcreator.yoruguamod.entity.BloquesinLEntity;

public class BloquesinLModel extends GeoModel<BloquesinLEntity> {
	@Override
	public ResourceLocation getAnimationResource(BloquesinLEntity entity) {
		return new ResourceLocation("yoruguamod", "animations/bloquesinl.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(BloquesinLEntity entity) {
		return new ResourceLocation("yoruguamod", "geo/bloquesinl.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(BloquesinLEntity entity) {
		return new ResourceLocation("yoruguamod", "textures/entities/" + entity.getTexture() + ".png");
	}

}
