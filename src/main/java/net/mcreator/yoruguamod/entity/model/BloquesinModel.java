package net.mcreator.yoruguamod.entity.model;

import software.bernie.geckolib.model.GeoModel;

import net.minecraft.resources.ResourceLocation;

import net.mcreator.yoruguamod.entity.BloquesinEntity;

public class BloquesinModel extends GeoModel<BloquesinEntity> {
	@Override
	public ResourceLocation getAnimationResource(BloquesinEntity entity) {
		return new ResourceLocation("yoruguamod", "animations/bloquesin.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(BloquesinEntity entity) {
		return new ResourceLocation("yoruguamod", "geo/bloquesin.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(BloquesinEntity entity) {
		return new ResourceLocation("yoruguamod", "textures/entities/" + entity.getTexture() + ".png");
	}

}
