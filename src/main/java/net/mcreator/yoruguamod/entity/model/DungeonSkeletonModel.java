package net.mcreator.yoruguamod.entity.model;

import software.bernie.geckolib.model.GeoModel;

import net.minecraft.resources.ResourceLocation;

import net.mcreator.yoruguamod.entity.DungeonSkeletonEntity;

public class DungeonSkeletonModel extends GeoModel<DungeonSkeletonEntity> {
	@Override
	public ResourceLocation getAnimationResource(DungeonSkeletonEntity entity) {
		return new ResourceLocation("yoruguamod", "animations/dungeon_skeleton_1.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(DungeonSkeletonEntity entity) {
		return new ResourceLocation("yoruguamod", "geo/dungeon_skeleton_1.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(DungeonSkeletonEntity entity) {
		return new ResourceLocation("yoruguamod", "textures/entities/" + entity.getTexture() + ".png");
	}

}
