package net.mcreator.yoruguamod.entity.model;

import software.bernie.geckolib.model.GeoModel;

import net.minecraft.resources.ResourceLocation;

import net.mcreator.yoruguamod.entity.ColosoEntity;

public class ColosoModel extends GeoModel<ColosoEntity> {
	@Override
	public ResourceLocation getAnimationResource(ColosoEntity entity) {
		return new ResourceLocation("yoruguamod", "animations/coloso.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(ColosoEntity entity) {
		return new ResourceLocation("yoruguamod", "geo/coloso.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(ColosoEntity entity) {
		return new ResourceLocation("yoruguamod", "textures/entities/" + entity.getTexture() + ".png");
	}

}
